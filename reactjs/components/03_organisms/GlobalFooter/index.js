import React from 'react';
import { Container, Row, Col } from 'reactstrap';

const GlobalFooter = () => (
  <footer>
    <Container>
      <Row>
        <Col>
          This is footer!
        </Col>
      </Row>
    </Container>
  </footer>
);

export default GlobalFooter;
